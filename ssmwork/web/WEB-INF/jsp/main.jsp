<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>首页</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">所有评论</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/user/tologin">后台</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${username}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/user/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">评论管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">发表评论</a></dd>
                        <dd><a href="javascript:;">修改评论</a></dd>
                        <dd><a href="javascript:;">删除评论</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">问题反馈</a></dd>
                        <dd><a href="javascript:;">平台反馈</a></dd>
                        <dd><a href="">超链接</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <div class="layui-tab layui-tab-brief" lay-filter="mainDemoTab">
                <ul class="layui-tab-title">
                    <li class="layui-this">已评论</li>
                    <li>未评论<span class="layui-badge">${userallnolist}</span></li>
                </ul>
                <div class="layui-tab-content" style="height: 100px;">
                    <div class="layui-tab-item layui-show">
                        <c:forEach var="comment" items="${requestScope.get('userqueryalllist')}">
                            <div style="margin: 10px 8% 15px 15px; height: 315px; position: relative;">
                                <div style="width: 100%; height: 175px;">
                                    <div class="layui-inline"
                                         style="height: 175px; width: 11%; padding-right: 15px; position: absolute;">
                                        <img src="${comment.getAdfilmImg()}" style="width: 100%; height: 100%;"/>
                                    </div>
                                    <div class="layui-inline"
                                         style="margin: 8px; width: 76%; position: absolute; left: 12%;">
                                        <p>
                                            片&emsp;&emsp;名：${comment.getAdfilmName()}
                                        </p>
                                        <p>
                                            简&emsp;&emsp;介：${comment.getAdfilmText()}
                                        </p>
                                        <div style="margin-left: 80%; margin-top: 5px;">
                                            <p>
                                                发布作者：${comment.getAdName()}
                                            </p>
                                            <p>
                                                发布时间：${comment.getAdfilmTime()}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="layui-inline" id="layerDemo"
                                         style="height: 175px; width: 7%; position: absolute;left: 90%;">
                                        <a href="${pageContext.request.contextPath}/user/toupdatecomment?commentId=${comment.getCommentId()}"
                                           class="layui-btn layui-btn-normal layui-btn-sm"
                                           style="margin: 5px auto; position: relative;left: 20px; top: 40px;">
                                            <i class="layui-icon"></i>
                                        </a>
                                        <br>
                                        <a href="${pageContext.request.contextPath}/user/deletecomment/${comment.getCommentId()}"
                                           class="layui-btn layui-btn-normal layui-btn-sm"
                                           style="margin: 5px auto; position: relative;left: 20px; top: 43px;">
                                            <i class="layui-icon"></i>
                                        </a>
                                    </div>
                                </div>
                                <div style="width: 99%; height: 125px; margin-top: 8px; border: 2px #00F7DE dotted; border-left: 7px #0099FF solid; border-right: 7px #0099FF solid; border-radius: 10px;">
                                    <div class="layui-inline" style="height: 100%; width: 82%; padding-left: 15px;">
                                        <p style="color: skyblue; font-weight: bold; font-size: 18px; text-align: center;">
                                            我的评论
                                        </p>
                                        <p>
                                            &emsp;&emsp;${comment.getCommentDiscuss()}
                                        </p>
                                    </div>
                                    <div class="layui-inline"
                                         style="width: 170px;height: 70px; position: relative; left: 40px;">
                                        <p>
                                            评论用户：${comment.getUserName()}
                                        </p>
                                        <p>
                                            评论时间：${comment.getCommentTime()}
                                        </p>
                                        <p style="color: orange; font-weight: bold; font-size: 18px;">
                                            评&emsp;分：${comment.getCommentScore()}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="layui-tab-item">
                        <c:forEach var="nocomment" items="${requestScope.get('userqueryallnolist')}">
                            <div style="margin: 10px 8% 15px 15px; height: 175px; position: relative;">
                                <div class="layui-inline"
                                     style="height: 100%; width: 11%; padding-right: 15px; position: absolute;">
                                    <img src="${nocomment.getAdfilmImg()}" style="width: 100%; height: 100%;"/>
                                </div>
                                <div class="layui-inline"
                                     style="margin: 8px; width: 76%; position: absolute; left: 12%;">
                                    <p>
                                        片&emsp;&emsp;名：${nocomment.getAdfilmName()}
                                    </p>
                                    <p>
                                        简&emsp;&emsp;介：${nocomment.getAdfilmText()}
                                    </p>
                                    <div style="margin-left: 80%; margin-top: 5px;">
                                        <p>
                                            发布作者：${nocomment.getAdName()}
                                        </p>
                                        <p>
                                            发布时间：${nocomment.getAdfilmTime()}
                                        </p>
                                    </div>
                                </div>
                                <div class="layui-inline" id="addCommentDemo"
                                     style="height: 100%; width: 7%; position: absolute;left: 90%;">
                                    <a href="${pageContext.request.contextPath}/user/toaddcomment?adfilmId=${nocomment.getAdfilmId()}"
                                       class="layui-btn layui-btn-normal layui-btn-sm"
                                       style="margin: 5px auto; position: relative;left: 20px;top: 70px;">
                                        <i class="layui-icon"></i>
                                    </a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use(['element', 'jquery'], function () {
        var element = layui.element;
        var $ = layui.jquery;
        var layer = layui.layer;

    });
</script>
</body>
</html>
