<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>后台管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
<%--    <base href="${basePath}">--%>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadmain">所有电影</a></li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadcomment">评论管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/tomain">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${adName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">电影</a>
                    <dl class="layui-nav-child">
                        <dd><a href="">发布电影</a></dd>
                        <dd><a href="" id="pupupdatefilm">更新电影</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">用户反馈</a></dd>
                        <dd><a href="javascript:;">评论反馈</a></dd>
                        <dd><a href="javascript:;">强制删除</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <form class="layui-form" action="${pageContext.request.contextPath}/administrators/addadfilm" method="post">
                <div class="layui-form-item">
                    <input type="hidden" name="adName" value="${adName}">
                    <div class="layui-inline">
                        <div class="layui-form-item">
                            <label class="layui-form-label">电影片名</label>
                            <div class="layui-input-block">
                                <input type="text" name="adfilmName" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input" style="width: 250px;">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">发布日期</label>
                            <div class="layui-input-inline">
                                <input type="text" name="adfilmTime" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input" style="width: 250px;">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="adfilmImgValue" name="adfilmImg" value="">
                    <div class="layui-upload layui-inline" style="margin: 0 50px;">
                        <button type="button" class="layui-btn" id="test1">上传图片</button>
                        <div class="layui-upload-list">
                            <img class="layui-upload-img" style="width: 92px; height: 92px;margin: 0 10px 10px 0;" id="demo1">
                            <p id="demoText"></p>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">电影简介</label>
                    <div class="layui-input-block">
                        <textarea placeholder="电影发展路线" class="layui-textarea" name="adfilmText" style="width: 670px; height: 210px;"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use(['element', 'form', 'layedit', 'laydate', 'upload'], function() {
        //主页
        var element = layui.element;
        //表单
        var form = layui.form,
            layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;
        //普通图片上传
        var $ = layui.jquery,
            upload = layui.upload;

        $('#pupupdatefilm').on('click',function () {
            layer.msg("亲！请先搜索电影");
        });

        //日期
        laydate.render({
            elem: '#date'
        });

        //监听提交
        form.on('submit(demo1)', function(data) {
            layer.msg("提交成功");
            // layer.alert(JSON.stringify(data.field), {
            //     title: '最终的提交信息'
            // });
            // $.ajax({
            //     url: '/administrators/addadfilm',
            //     data: data.field,
            //     success: function (data) {
            //         layer.msg("提交成功")
            //     }
            // });
            // return false;
        });

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#test1',
            url: '/administrators/uploadImg' //改成您自己的上传接口
            ,
            before: function(obj) {
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result) {
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            },
            done: function(res) {
                //如果上传失败
                if (res.code > 0) {
                    return layer.msg('上传失败');
                }
                //上传成功
                console.log(res.data.src);
                $('#adfilmImgValue').val(res.data.src);
            },
            error: function() {
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html(
                    '<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function() {
                    uploadInst.upload();
                });
            }
        });
    });
</script>
</body>
</html>
