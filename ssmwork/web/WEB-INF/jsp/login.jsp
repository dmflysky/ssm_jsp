<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>管理员登录</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
            background: #34495e;
        }
        .box {
            width: 300px;
            padding: 40px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background: #191919;
            text-align: center;
        }
        .box h1 {
            color: white;
            text-transform: uppercase;
            font-weight: 500;
        }
        .box input[type='text'],
        .box input[type='password'] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #3498db;
            padding: 14px 10px;
            width: 200px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
        }
        .box input[type='text']:focus,
        .box input[type='password']:focus {
            width: 280px;
            border-color: #2ecc71;
        }
        .submit {
            border: 0;
            background: none;
            margin: 20px auto;
            margin-top: 0;
            display: inline-block;
            text-align: center;
            border: 2px solid #3498db;
            padding: 10px 40px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
            cursor: pointer;
            text-decoration: none;
            font-size: 12px;
        }
        .submit:hover {
            background: #2ecc71;
            border-color: #2ecc71;
        }
    </style>
</head>
<body>
<div class="box">
    <form id="ad-form" action="${pageContext.request.contextPath}/administrators/login" method="post">
        <h1>Login</h1>
        <input type="text" placeholder="Username" name="adName" autofocus required="true"/>
<%--        <span style="color: red;font-weight: bold;float: left">${adnameerror}</span>--%>
        <input type="password" placeholder="Password" name="adPassword" required="true"/>
<%--        <span style="color: red;font-weight: bold;float: left">${adpwerror}</span>--%>
        <a class="submit" onclick="adform()">Login</a>
    </form>
</div>
<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
    $(function() {
        window.error();
        window.adform = function() {
            document.getElementById('ad-form').submit();
        };
    });
    function error() {
        var adnameerror = "${adnameerror}", adpwerror = "${adpwerror}";
        console.log(adnameerror);
        console.log(adpwerror);
        if (adnameerror==""&&adpwerror==""){
            return;
        }else if (adnameerror!=""){
            console.log(adnameerror);
            alert(adnameerror);
        }else if (adnameerror==""&&adpwerror!=""){
            console.log(adpwerror);
            alert(adpwerror);
        }
    }
</script>
</body>
</html>
