<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>用户更新评论</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">所有评论</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/user/tologin">后台</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${username}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/user/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">评论管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">发表评论</a></dd>
                        <dd><a href="javascript:;">修改评论</a></dd>
                        <dd><a href="javascript:;">删除评论</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">问题反馈</a></dd>
                        <dd><a href="javascript:;">平台反馈</a></dd>
                        <dd><a href="">超链接</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
                <div style="margin: 10px 8% 15px 15px; height: 180px; position: relative;">
                    <div style="width: 100%; height: 175px;">
                        <div class="layui-inline"
                             style="height: 175px; width: 11%; padding-right: 15px; position: absolute;">
                            <img src="${querycommentlist.getAdfilmImg()}" style="width: 100%; height: 100%;"/>
                        </div>
                        <div class="layui-inline"
                             style="margin: 8px; width: 76%; position: absolute; left: 12%;">
                            <p>
                                片&emsp;&emsp;名：${querycommentlist.getAdfilmName()}
                            </p>
                            <p>
                                简&emsp;&emsp;介：${querycommentlist.getAdfilmText()}
                            </p>
                            <div style="margin-left: 80%; margin-top: 5px;">
                                <p>
                                    发布作者：${querycommentlist.getAdName()}
                                </p>
                                <p>
                                    发布时间：${querycommentlist.getAdfilmTime()}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="layui-form" action="${pageContext.request.contextPath}/user/updatecomment" method="post">
                    <div class="layui-form-item">
                        <input type="hidden" name="commentId" value="${querycommentlist.getCommentId()}">
                        <input type="hidden" name="userName" value="${username}">
                        <input type="hidden" name="adfilmImg" value="${querycommentlist.getAdfilmImg()}">
                        <input type="hidden" name="adfilmName" value="${querycommentlist.getAdfilmName()}">
                        <input type="hidden" name="adfilmText" value="${querycommentlist.getAdfilmText()}">
                        <input type="hidden" name="adName" value="${querycommentlist.getAdName()}">
                        <input type="hidden" name="adfilmTime" value="${querycommentlist.getAdfilmTime()}">
                        <div class="layui-form-item layui-form-text">
                            <label class="layui-form-label">我的评论</label>
                            <div class="layui-input-block">
                                <textarea class="layui-textarea" name="commentDiscuss"
                                          style="width: 670px; height: 180px;">
                                        ${querycommentlist.getCommentDiscuss()}
                                </textarea>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-label">评论时间</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="commentTime" id="date" lay-verify="date"
                                           placeholder="${querycommentlist.getCommentTime()}" autocomplete="off"
                                           class="layui-input" style="width: 260px;">
                                </div>
                            </div>
                            <div class="layui-inline" style=" margin-left: 85px;">
                                <label class="layui-form-label">评分</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="commentScore" lay-verify="title" autocomplete="off"
                                           placeholder="${querycommentlist.getCommentScore()}" class="layui-input"
                                           style="width: 260px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button type="submit" class="layui-btn" lay-submit="" lay-filter="updateform">立即提交</button>
                            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use(['element', 'form', 'layedit', 'laydate'], function () {
        //主页
        var element = layui.element;
        //表单
        var form = layui.form,
            layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;
        var $ = layui.jquery;

        //日期
        laydate.render({
            elem: '#date',
            type: 'date',
            trigger: 'click'
        });

        //监听提交
        form.on('submit(updateform)', function (data) {
            layer.msg("提交成功");
            // layer.alert(JSON.stringify(data.field), {
            //     title: '最终的提交信息'
            // });
            // $.ajax({
            //     url: '/administrators/addadfilm',
            //     data: data.field,
            //     success: function (data) {
            //         layer.msg("提交成功")
            //     }
            // });
            // return false;
        });
    });
</script>
</body>
</html>
