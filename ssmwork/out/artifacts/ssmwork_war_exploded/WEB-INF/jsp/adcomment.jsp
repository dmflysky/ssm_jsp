<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css" media="all">
    <title>评论管理</title>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadmain">所有电影</a>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadcomment">评论管理</a>
            </li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/tomain">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${adName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">电影</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/administrators/toaddfilm">发布电影</a></dd>
                        <dd><a href="" id="pupupdatefilm">更新电影</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">用户反馈</a></dd>
                        <dd><a href="javascript:;">评论反馈</a></dd>
                        <dd><a href="javascript:;">强制删除</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
                <ul class="layui-tab-title">
                    <li class="layui-this">所有评论</li>
                    <li>图文信息</li>
                </ul>
                <div class="layui-tab-content" style="height: 100px;">
                    <div class="layui-tab-item layui-show">
                        <table class="layui-hide" id="test"></table>
                    </div>
                    <div class="layui-tab-item">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['element', 'form', 'table', 'jquery'], function () {
        var element = layui.element;
        var form = layui.form;
        var table = layui.table;
        var $ = layui.jquery;
        var layer = layui.layer;

        table.render({
            elem: '#test',
            url: '${pageContext.request.contextPath}/administrators/tableadcomment',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 1, //只显示 1 个连续页码
                first: false, //不显示首页
                last: false //不显示尾页
            },
            cols: [[
                {field: 'commentId', width: 80, title: 'ID', sort: true},
                {field: 'userName', width: 90, title: '用户'},
                {field: 'commentScore', width: 80, title: '评分', sort: true},
                {field: 'commentDiscuss', title: '影评', minWidth: 150},
                {
                    field: 'commentTime',
                    width: 115,
                    title: '发表时间',
                    templet: "<div>{{layui.util.toDateString(d.commentTime, 'yyyy-MM-dd')}}</div>"
                },
                {field: 'adfilmName', width: 115, title: '片名'},
                {field: 'adName', width: 90, title: '发布者'},
                {
                    field: 'adfilmTime',
                    width: 115,
                    title: '发布时间',
                    templet: "<div>{{layui.util.toDateString(d.adfilmTime, 'yyyy-MM-dd')}}</div>"
                }
            ]]
        });

    });
</script>
</body>
</html>
