<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>后台管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadmain">所有电影</a>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadcomment">评论管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/tomain">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${adName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">电影</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/administrators/toaddfilm">发布电影</a></dd>
                        <dd><a href="" id="pupupdatefilm">更新电影</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">用户反馈</a></dd>
                        <dd><a href="javascript:;">评论反馈</a></dd>
                        <dd><a href="javascript:;">强制删除</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <div class="layui-row">
                <div class="layui-col-md5 layui-col-md-offset3 layuib">
                    <form class="layui-form" action="">
                        <div class="layui-form-item layui-inline">
                            <label class="layui-form-label" style="color: red;font-weight: bold">${error}</label>
                            <div class="layui-inline">
                                <input type="hidden" name="adName" value="${adName}">
                                <input type="text" name="adfilmName" placeholder="请输入查询的电影" autocomplete="off"
                                       class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item layui-inline">
                            <button type="submit" class="layui-btn layui-btn-normal" lay-submit=""
                                    lay-filter="formDemo">搜索
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <c:forEach var="adfilm" items="${requestScope.get('querylist')}">
                <div style="margin: 10px 8% 15px 15px; height: 175px; position: relative;">
                    <div class="layui-inline" style="height: 100%; width: 11%; padding-right: 15px; position: absolute;">
                        <img src="${adfilm.getAdfilmImg()}" style="width: 100%; height: 100%;"/>
                    </div>
                    <div class="layui-inline" style="margin: 8px; width: 76%; position: absolute; left: 12%;">
                        <p>
                            片&emsp;&emsp;名：${adfilm.getAdfilmName()}
                        </p>
                        <p>
                            简&emsp;&emsp;介：${adfilm.getAdfilmText()}
                        </p>
                        <div style="margin-left: 80%; margin-top: 5px;">
                            <p>
                                发布作者：${adfilm.getAdName()}
                            </p>
                            <p>
                                发布时间：${adfilm.getAdfilmTime()}
                            </p>
                        </div>
                    </div>
                    <div class="layui-inline" id="layerDemo" style="height: 100%; width: 7%; margin: 20px; position: absolute;left: 90%;">
                        <button type="button" class="layui-btn layui-btn-normal layui-btn-sm" style="margin: 5px auto"><i class="layui-icon"></i></button><br>
                        <button type="button" data-method="notice" data-type="auto" class="layui-btn layui-btn-normal layui-btn-sm"
                                style="margin: 5px auto"><i class="layui-icon"></i></button><br>
                        <button type="button" class="layui-btn layui-btn-normal layui-btn-sm" style="margin: 5px auto"><i class="layui-icon"></i></button>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>

<%--update弹窗div--%>
<script type="text/html" id="popUpdateadfilm">
    <c:forEach var="adfilms" items="${requestScope.get('querylist')}">
        <div class="layui-row">
            <form class="layui-form" action="" method="post" style="padding: 15px;">
                <div class="layui-form-item">
                    <input type="hidden" name="adName" value="${adName}">
                    <input type="hidden" name="adfilmId" value="${adfilms.getAdfilmId()}">
                    <div class="layui-inline">
                        <div class="layui-form-item">
                            <label class="layui-form-label">电影片名</label>
                            <div class="layui-input-block">
                                <input type="text" name="adfilmName" lay-verify="title" autocomplete="off"
                                       placeholder="${adfilms.getAdfilmName()}" class="layui-input"
                                       style="width: 250px;">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">发布日期</label>
                            <div class="layui-input-inline">
                                <input type="text" name="adfilmTime" id="date" lay-verify="date"
                                       placeholder="${adfilms.getAdfilmTime()}" autocomplete="off"
                                       class="layui-input" style="width: 250px;">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="adfilmImgValue" name="adfilmImg" value="">
                    <div class="layui-upload layui-inline" style="margin: 0 50px;">
                        <button type="button" class="layui-btn" id="test1">上传图片</button>
                        <div class="layui-upload-list">
                            <img class="layui-upload-img" src="${adfilms.getAdfilmImg()}"
                                 style="width: 92px; height: 92px;margin: 0 10px 10px 0;"
                                 id="demo1">
                            <p id="demoText"></p>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">电影简介</label>
                    <div class="layui-input-block">
                        <textarea class="layui-textarea" name="adfilmText" style="width: 670px; height: 185px;">
                            ${adfilms.getAdfilmText()}
                        </textarea>
                    </div>
                </div>
            </form>
        </div>
    </c:forEach>
</script>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use(['element', 'form', 'layedit', 'laydate', 'upload', 'jquery'], function () {
        var element = layui.element;
        //表单
        var form = layui.form,
            layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;
        //普通图片上传
        var $ = layui.jquery,
            upload = layui.upload;

        $('#pupupdatefilm').on('click',function () {
            layer.msg("亲！您已在更新电影页面，可以更新电影");
        });

        //弹窗更新form触发事件
        var active = {
            notice: function () {
                //示范一个公告层
                layer.open({
                    //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层)
                    type: 1,
                    //显示标题栏
                    title: ['更新电影', 'font-size:18px;'],
                    //弹窗皮肤
                    skin: 'layui-layer-molv',
                    closeBtn: 1,
                    //定义宽高
                    area: ['850px', '505px'],
                    //遮幕
                    shade: 0.8,
                    //设定一个id，防止重复弹出
                    id: 'LAY_layuipro',
                    btn: ['更新', '取消'],
                    //按钮排列：居中对齐
                    btnAlign: 'c',
                    //拖拽模式，0或者1
                    moveType: 1,
                    //<%--显示界面     跳转到想要的界面，'${path}/toAddAd'--%>
                    content: $("#popUpdateadfilm").html(),
                    success: function (layero, index) {
                        form.render();
                        layui.laydate.render({
                            elem: '#date',
                        });
                        //普通图片上传
                        var uploadInst = upload.render({
                            elem: '#test1',
                            //上传接口
                            url: '${pageContext.request.contextPath}/administrators/uploadImg',
                            before: function (obj) {
                                //预读本地文件示例，不支持ie8
                                obj.preview(function (index, file, result) {
                                    $('#demo1').attr('src', result); //图片链接（base64）
                                });
                            },
                            done: function (res) {
                                //如果上传失败
                                if (res.code > 0) {
                                    return layer.msg('上传失败');
                                }
                                //上传成功
                                console.log(res.data.src);
                                $('#adfilmImgValue').val(res.data.src);
                            },
                            error: function () {
                                //演示失败状态，并实现重传
                                var demoText = $('#demoText');
                                demoText.html(
                                    '<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                                demoText.find('.demo-reload').on('click', function () {
                                    uploadInst.upload();
                                });
                            }
                        });

                        <%--if ($('#adfilmImgValue').val(res.data.src)==null){--%>
                        <%--    $('#adfilmImgValue').val(${adfilms.getAdfilmImg()});--%>
                        <%--}--%>

                        // 解决按enter键重复弹窗问题
                        $(':focus').blur();
                        layero.addClass('layui-form');
                        // 将保存按钮改变成提交按钮
                        layero.find('.layui-layer-btn0').attr({
                        // var btn = layero.find('.layui-layer-btn');
                        // btn.find('.layui-layer-btn0').attr({
                            'lay-filter': 'updateform',
                            'lay-submit': ''
                            <%--href: '${path}',--%>
                            <%--href: '${pageContext.request.contextPath}/administrators/queryadfilm',--%>
                            <%--target: '_top'--%>
                        });
                    },
                    yes: function (index, layero) { // 确认按钮回调函数
                        // 监听提交按钮
                        form.on('submit(updateform)', function (data) {
                            $.ajax({
                                url: '/administrators/updateadfilm',
                                data: data.field,
                                success: function (data) {
                                    layer.msg("更新成功");
                                }
                            });
                            layer.close(index);
                        });
                    },
                    btn2 : function(index, layero) { // 取消按钮回调函数
                        layer.close(index); // 关闭弹出层
                    }
                });
            }
        };
        $('#layerDemo .layui-btn').on('click', function () {
            var othis = $(this),
                method = othis.data('method');
            active[method] ? active[method].call(this, othis) : '';
        });

        //搜索查询监听提交
        form.on('submit(formDemo)', function (data) {
            layer.msg("搜索成功");
            // return false;
        });
    });
</script>
</body>
</html>