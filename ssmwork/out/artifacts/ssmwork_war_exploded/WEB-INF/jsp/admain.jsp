<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>后台管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">影评管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadmain">所有电影</a></li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/toadcomment">评论管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/tomain">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${adName}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/administrators/logout">退了</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">电影</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/administrators/toaddfilm">发布电影</a></dd>
                        <dd><a href="" id="pupupdatefilm">更新电影</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">用户反馈</a></dd>
                        <dd><a href="javascript:;">评论反馈</a></dd>
                        <dd><a href="javascript:;">强制删除</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="">云市场</a></li>
                <li class="layui-nav-item"><a href="">发布商品</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <div class="layui-row">
                <div class="layui-col-md5 layui-col-md-offset3 layuib">
                    <form class="layui-form" action="${pageContext.request.contextPath}/administrators/queryadfilm">
                        <div class="layui-form-item layui-inline">
                            <label class="layui-form-label" style="color: red;font-weight: bold">${error}</label>
                            <div class="layui-inline">
                                <input type="hidden" name="adName" value="${adName}">
                                <input type="text" name="adfilmName" placeholder="请输入查询的电影" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item layui-inline">
                            <button type="submit" class="layui-btn layui-btn-normal" lay-submit="" lay-filter="formDemo">搜索</button>
                        </div>
                    </form>
                </div>
            </div>
            <c:forEach var="adfilms" items="${requestScope.get('adlist')}">
                <div style="padding: 10px 200px 10px 15px; height: 172px;">
                    <div style="height: 100%; width: 132px; float: left;padding-right: 20px;">
                        <img src="${adfilms.getAdfilmImg()}" style="width: 100%; height: 100%;"/>
                    </div>
                    <div style="margin: 10px;">
                        <p>
                            片&emsp;&emsp;名：${adfilms.getAdfilmName()}
                        </p>
                        <p>
                            简&emsp;&emsp;介：${adfilms.getAdfilmText()}
                        </p>
                        <div style="margin-left: 82%; margin-top: 10px;">
                            <p>
                                发布作者：${adfilms.getAdName()}
                            </p>
                            <p>
                                发布时间：${adfilms.getAdfilmTime()}
                            </p>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use(['element','form' ,'jquery'], function () {
        var element = layui.element;
        var form = layui.form;
        var $ = layui.jquery;
        var layer = layui.layer;
        //监听提交

        $('#pupupdatefilm').on('click',function () {
            layer.msg("亲！请先搜索电影");
        });

        form.on('submit(formDemo)', function(data){
            layer.msg("搜索成功");
            // layer.alert(JSON.stringify(data.field), {
            //     title: '最终的提交信息'
            // });
            // $.ajax({
            //     url: '/administrators/queryadfilm',
            //     data: data.field,
            //     success: function (data) {
            //         layer.msg("搜索成功")
            //     }
            // });
            //return false;
        });
    });
</script>
</body>
</html>
