<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录/注册</title>
    <style>
        * {
            padding: 0px;
            margin: 0px;
        }

        html, body {
            height: 100%;
            width: 100%;
            background-image: linear-gradient(120deg, #487eb0, #fbc531);
            overflow: hidden;
        }

        .container {
            position: absolute;
            height: 350px;
            width: 600px;
            background-color: rgba(255, 255, 255, .9);
            top: 50%;
            left: 50%;
            transform: translate(-50%, -30%);
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(17, 39, 59, 0.8);
            border: 1px solid rgba(17, 39, 59, 1);
            overflow: hidden;
            box-sizing: border-box;
            opacity: 0;
            transition: 1s;
        }

        .container-show {
            transform: translate(-50%,-50%) ;
            opacity: 1;
        }

        .container:hover .title {
            font-size: 20px;
            /* transform: translate(0,-30%); */
        }

        .container:hover input {
            transform: translate(0, -30%);
        }

        .container:hover .btn {
            height: 30px;
            width: 90px;
            transform: translate(0,-30%);
            font-size: 12px;
        }

        .container:hover .change-box {
            height: 200px;
        }

        .container:hover .change-btn {
            display: block;
        }

        .title {
            margin-top: 10px;
            position: relative;
            height: 40px;
            width: 100%;
            font-size: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-transform: uppercase;
            font-weight: 500;
            letter-spacing: 3px;
            transition: .4s;
        }
        .login-box .title {
            color: white;
            text-shadow: 0px 0px 7px rgba(255, 255, 255, .9);
        }

        .input {
            width: 400px;
            height: 45px;
            position: relative;
            margin: 40px auto;
            /* border-radius: 45px;
            overflow: hidden; */
        }

        input {
            position: relative;
            width: 100%;
            height: 100%;
            border: none;
            outline: none;
            /* box-sizing: border-box; */
            padding-left: 15px;
            font-size: 16px;
            background-color: rgba(255, 255, 255, 0.4);
            border-radius: 45px;
            transition: .4s;
        }

        .login-box input:focus {
            box-shadow: 0 0 10px rgba(1,1,1, .8);
        }

        .btn {
            height: 50px;
            width: 160px;
            position: relative;
            margin: -10px auto;
            background-color: rgba(0, 0, 0, 0.1);
            border-radius: 20px;
            color: rgba(255, 255, 255, .4);
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: pointer;
            transition: .4s;
        }

        .change-box {
            position: absolute;
            height: 0px;
            width: 100%;
            clip-path: polygon(100% 50%, 50% 100%, 100% 100%);
            bottom: 0px;
            transition: .4s;
        }

        .change-btn {
            position: absolute;
            bottom: 30px;
            right: 40px;
            font-size: 20px;
            display: none;
            font-weight: 500;
        }
        .change-btn:hover {
            text-shadow: 0px 0px 3px rgba(200, 200, 200, .8);
            cursor: pointer;
        }

        .login-box input {
            caret-color: white;
            color: rgba(255, 255, 255, 0.8);
        }

        .login-btn:hover {
            color: white;
            background-color: #57606f;
            box-shadow: rgba(0, 0, 0, 0.1);
            text-shadow: 0px 0px 2px rgba(0, 0, 0, 0.1);
        }

        .login-change {
            background-color: rgba(255, 255, 255, .8);
        }

        .animate_login {
            transform: rotate(-90deg);
        }

        .animate_sign {
            transform: rotate(0deg) !important;
        }



        .login-box {
            position: absolute;
            height: 100%;
            width: 100%;
            background-color: rgba(17, 39, 59, 0.8);
            transition: .4s;
            z-index: 1;
            transform-origin: 0 100%;
        }



        /* 注册框样式 */
        .sign-box {
            position: absolute;
            height: 100%;
            width: 100%;
            background-color: rgba(255, 255, 255, .8);
            transform-origin: 0 100%;
            transform: rotate(90deg);
            transition: .4s;
            z-index: 1;
        }

        .sign-box  .title {
            margin-bottom: 35px;
            text-shadow: 0 0 10px rgba(200, 200, 200, .8);
            font-weight: 500;
        }

        .sign-change {
            background-color: rgba(17, 39, 59, 0.8);
        }

        .toLogin {
            color: white;
        }

        .sign-box .input{
            width: 40%;
            height: 45px;
            margin: 0px 25px 40px;
            display: inline-block;
        }

        .sign-box input {
            box-shadow: 0 0 3px black;
        }

        .sign-box .btn {
            color: #1e90ff;
            background-color: rgba(200, 200, 200, .1);
            transition:.5s;
        }

        .sign-box .btn:hover {
            color: white;
            background-color: #1e90ff;
        }

        /* Mac 按钮样式及特效 */
        .apple-btn {
            position: absolute;
            height: 15px;
            width: 65px;
            top: 3px;

        }
        .apple-btn li{
            list-style: none;
            position: relative;
            height: 15px;
            width: 15px;
            border-radius: 15px;
            opacity: 0;
        }

        .login-apple li{
            left: 5px;
            float: left;
        }

        .sign-apple li {
            right: 5px;
            float: right;
        }

        .sign-apple {
            right: 5px;
        }

        li:nth-child(2) {
            margin: 0px 2px
        }

        .red-btn {
            background-color: red;
            transition: .3s;
            transform: translate(0, -50%);
        }

        .yellow-btn {
            background-color: orange;
            /* transition-delay: .2s; */
            transition: .6s;
            transform: translate(0, -50%);
        }

        .green-btn {
            background-color: rgb(15, 136, 15);
            /* transition-delay: .3s; */
            transition: .9s;
            transform: translate(0, -50%);
        }

        .container:hover .red-btn {
            opacity: 1;
            transform: translate(0,0);
        }
        .container:hover .yellow-btn {
            opacity: 1;
            transform: translate(0,0);
        }
        .container:hover .green-btn {
            opacity: 1;
            transform: translate(0,0);
        }
    </style>
</head>
<body>

<div class="container">
    <div class="login-box">
        <div class="apple-btn login-apple">
            <li class="red-btn"></li>
            <li class="yellow-btn"></li>
            <li class="green-btn"></li>
        </div>
        <form action="${pageContext.request.contextPath}/user/index" id="login-form" method="get">
            <div class="title">Login</div>

            <div class="input">
                <input type="text" id="login-user" name="username" placeholder="Input your username" autofocus required="true"/>
                <span style="color: red;font-weight: bold">${usererror}</span>
            </div>

            <div class="input">
                <input type="password" id="login-password" name="userpassword" placeholder="Input your password" required="true"/>
                <span style="color: red;font-weight: bold">${pwerror}</span>
            </div>
            <div class="btn login-btn">
                <span onclick="loginform()">登录</span>
            </div>
            <div class="change-box login-change">
                <div class="change-btn toSign">
                    <span>去注册</span>
                </div>
            </div>
        </form>
    </div>

    <div class="sign-box">
        <div class="apple-btn sign-apple">
            <li class="red-btn"></li>
            <li class="yellow-btn"></li>
            <li class="green-btn"></li>
        </div>
        <form action="${pageContext.request.contextPath}/user/register" id="sign-form" method="get">
            <div class="title">Sign</div>
            <div class="input">
                <input type="text" id="user" name="userName" placeholder="Have A Good Name?" autofocus required="true"/>
            </div>
            <div class="input">
                <input type="password" id="password" name="userPassword" placeholder="There's a secure password" required="true"/>
            </div>
            <div class="input">
                <input type="text" id="phone" name="userPhone" pattern="^1[3|4|5|7|8][0-9]{9}$" placeholder="Nice mobile numbert" required="true"/>
            </div>
            <div class="input">
                <input type="text" id="email" name="userEmail" pattern="^[a-z0-9A-Z]+[- | a-z0-9A-Z . _]+@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]{2,}$" placeholder="Rich instant email" required="true"/>
            </div>
            <div class="btn sign-btn">
                <span onclick="signform()">注册</span>
            </div>
        </form>
        <div class="change-box sign-change">
            <div class="change-btn toLogin">
                <span>去登录</span>
            </div>
        </div>
    </div>
</div>

<script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
    $(function() {
        window.loginform = function() {
            document.getElementById('login-form').submit();
        };
        window.signform = function() {
            document.getElementById('sign-form').submit();
        };
    });

    const getSelector = ele => {
        return typeof ele === "string" ? document.querySelector(ele) : "";
    };
    // 登录注册载入
    const containerShow = () => {
        var show = getSelector(".container")
        show.className += " container-show"
    };
    window.onload = containerShow;
    // 登录注册页切换
    ((window, document) => {
        // 登录 -> 注册
        let toSignBtn = getSelector(".toSign"),
            toLoginBtn = getSelector(".toLogin")
        loginBox = getSelector(".login-box"),
            signBox = getSelector(".sign-box");
        toSignBtn.onclick = () => {
            loginBox.className += ' animate_login';
            signBox.className += ' animate_sign';
        }
        toLoginBtn.onclick = () => {
            loginBox.classList.remove("animate_login");
            signBox.classList.remove("animate_sign");
        }
    })(window, document);
</script>
</body>
</html>
