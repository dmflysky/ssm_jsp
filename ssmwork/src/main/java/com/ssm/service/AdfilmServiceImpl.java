package com.ssm.service;

import com.ssm.dao.AdfilmMapper;
import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Comments;
import java.util.List;
import java.util.Map;

public class AdfilmServiceImpl implements AdfilmService {

    private AdfilmMapper adfilmMapper;

    public void setAdfilmMapper(AdfilmMapper adfilmMapper) {
        this.adfilmMapper = adfilmMapper;
    }

    @Override
    public int addAdfilm(Adfilms adfilms) {
        return adfilmMapper.addAdfilm(adfilms);
    }

    @Override
    public int updateAdfilm(Adfilms adfilms) {
        return adfilmMapper.updateAdfilm(adfilms);
    }

    @Override
    public Adfilms queryAdfilmById(int adfilmId) {
        return adfilmMapper.queryAdfilmById(adfilmId);
    }

    @Override
    public List<Adfilms> queryAllAdfilm(String adName) {
        return adfilmMapper.queryAllAdfilm(adName);
    }

    @Override
    public Adfilms queryAdfilmName(Map map) {
        return adfilmMapper.queryAdfilmName(map);
    }

    @Override
    public List<Comments> queryAllComment(String adName) {
        return adfilmMapper.queryAllComment(adName);
    }

}
