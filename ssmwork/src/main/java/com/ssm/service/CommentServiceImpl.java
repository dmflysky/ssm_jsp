package com.ssm.service;

import com.ssm.dao.CommentMapper;
import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Comments;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public class CommentServiceImpl implements CommentService{

    private CommentMapper commentMapper;

    public void setCommentMapper(CommentMapper commentMapper) {
        this.commentMapper = commentMapper;
    }

    @Override
    public int addComment(Comments comments) {
        return commentMapper.addComment(comments);
    }

    @Override
    public int deleteCommentById(int commentId) {
        return commentMapper.deleteCommentById(commentId);
    }

    @Override
    public int updateComment(Comments comments) {
        return commentMapper.updateComment(comments);
    }

    @Override
    public Adfilms queryAdfilmById(int adfilmId) {
        return commentMapper.queryAdfilmById(adfilmId);
    }

    @Override
    public Comments queryCommentById(int commentId) {
        return commentMapper.queryCommentById(commentId);
    }

    @Override
    public List<Comments> queryAllComment(@Param("userName") String userName) {
        return commentMapper.queryAllComment(userName);
    }

    @Override
    public List<Adfilms> queryAllnoComment(@Param("userName") String userName) {
        return commentMapper.queryAllnoComment(userName);
    }
}
