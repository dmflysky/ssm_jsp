package com.ssm.service;

import com.ssm.dao.UserMapper;
import com.ssm.pojo.User;

public class UserServiceImpl implements UserService{

    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public User queryUserName(String userName) {
        return userMapper.queryUserName(userName);
    }
}
