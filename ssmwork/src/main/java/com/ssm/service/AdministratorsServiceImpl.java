package com.ssm.service;

import com.ssm.dao.AdministratorsMapper;
import com.ssm.pojo.Administrators;

public class AdministratorsServiceImpl implements AdministratorsService{

    private AdministratorsMapper administratorsMapper;

    public void setAdministratorsMapper(AdministratorsMapper administratorsMapper) {
        this.administratorsMapper = administratorsMapper;
    }

    @Override
    public Administrators queryAdministratorsName(String adName) {
        return administratorsMapper.queryAdministratorsName(adName);
    }
}
