package com.ssm.dao;

import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Comments;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {

    //增加一个Comment
    int addComment(Comments comments);

    //根据id删除一个Comment
    int deleteCommentById(int commentId);

    //更新Comment
    int updateComment(Comments comments);

    //根据id查询,返回一个Adfilm
    Adfilms queryAdfilmById(int adfilmId);

    //根据id查询,返回一个Comment
    Comments queryCommentById(int commentId);

    //查询全部已评论Comment,返回list集合
    List<Comments> queryAllComment(@Param("userName") String userName);

    //查询全部未评论Comment,返回list集合
    List<Adfilms> queryAllnoComment(@Param("userName") String userName);
}
