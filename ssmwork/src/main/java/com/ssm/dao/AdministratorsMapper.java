package com.ssm.dao;

import com.ssm.pojo.Administrators;
import org.apache.ibatis.annotations.Param;

public interface AdministratorsMapper {
    //根据用户名查询 返回User
    Administrators queryAdministratorsName(@Param("adName") String adName);
}
