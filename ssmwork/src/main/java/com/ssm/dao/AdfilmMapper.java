package com.ssm.dao;

import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Comments;

import java.util.List;
import java.util.Map;

public interface AdfilmMapper {

    //增加一个Adfilm
    int addAdfilm(Adfilms adfilms);

    //更新Adfilm
    int updateAdfilm(Adfilms adfilms);

    //根据id查询,返回一个Adfilm
    Adfilms queryAdfilmById(int adfilmId);

    //查询全部Adfilm,返回list集合
    List<Adfilms> queryAllAdfilm(String adName);

    //搜索查询
    Adfilms queryAdfilmName(Map map);

    //查询全部Comments,返回list集合
    List<Comments> queryAllComment(String adName);

}
