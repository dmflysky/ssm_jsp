package com.ssm.dao;

import com.ssm.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    //注册用户
    int addUser(User user);

    //根据用户名查询 返回User
    User queryUserName(@Param("userName") String userName);
}
