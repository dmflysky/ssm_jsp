package com.ssm.controller;

import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Administrators;
import com.ssm.pojo.Comments;
import com.ssm.pojo.LayuiTypeJson;
import com.ssm.service.AdfilmService;
import com.ssm.service.AdministratorsService;
import com.ssm.service.CommentService;
import org.apache.ibatis.mapping.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/administrators")
    public class AdministratorsController {

    @Autowired
    @Qualifier("AdministratorsServiceImpl")
    private AdministratorsService administratorsService;

    @Autowired
    @Qualifier("AdfilmServiceImpl")
    private AdfilmService adfilmService;

    @Autowired
    @Qualifier("CommentServiceImpl")
    private CommentService commentService;

    //返回用户主页
    @RequestMapping("/tomain")
    public String toMain(HttpSession session,Model model) {
        //获取session中用户名
        String userName = (String) session.getAttribute("username");
        //获取所有已评论的影评，返回list集合
        List<Comments> userqueryalllist = commentService.queryAllComment(userName);
        //获取所有未评论的影评，返回list集合
        List<Adfilms> userqueryallnolist = commentService.queryAllnoComment(userName);
        //获取有未评论的影评的数量
        int userallnolist = userqueryallnolist.size();
        model.addAttribute("userqueryalllist", userqueryalllist);
        model.addAttribute("userqueryallnolist", userqueryallnolist);
        model.addAttribute("userallnolist", userallnolist);
        return "main";
    }

    //返回管理员主页，并查询所有发布电影
    @RequestMapping("/toadmain")
    public String toAdMain(HttpSession session,Model model) {
        String ADName = (String) session.getAttribute("adName");
        List<Adfilms> adlist = adfilmService.queryAllAdfilm(ADName);
        model.addAttribute("adlist", adlist);
        return "admain";
    }

    //登陆提交
    @RequestMapping("/login")
    public String login(HttpSession session, String adName, String adPassword, Model model) throws Exception {
        // 向session记录用户身份信息
        session.setAttribute("adName", adName);
        session.setAttribute("adPassword", adPassword);
        Administrators administrators = administratorsService.queryAdministratorsName(adName);
        List<Administrators> list = new ArrayList<Administrators>();
        list.add(administrators);
        if (administrators == null) {
            model.addAttribute("adnameerror", "账号不存在，请你注册或重新输入");
            return "login";
        } else if (administrators.getAdPassword().equals(adPassword)) {
            return "redirect:/administrators/toadmain";
        } else {
            model.addAttribute("adnameerror", "");
            model.addAttribute("adpwerror", "密码错误，请你重新输入");
            return "login";
        }
    }

    //退出登陆
    @RequestMapping("logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.removeAttribute("adName");
        session.removeAttribute("adPassword");
        return "main";
    }

    //跳转到addfilm页面
    @RequestMapping("/toaddfilm")
    public String toAddfilm() {
        return "addfilm";
    }

    @ResponseBody
    @RequestMapping(value = "/uploadImg", method = {RequestMethod.POST})
    public Object fileUpload(@RequestParam("file") CommonsMultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println(file.getOriginalFilename());
        //上传路径保存设置
        String path = request.getSession().getServletContext().getRealPath("static/images/");
        File pathFile = new File(path);
        if (!pathFile.exists()){
            pathFile.mkdir();
        }
        //上传文件地址
        UUID uuid = UUID.randomUUID();
        System.out.println("上传文件保存地址："+pathFile.getAbsolutePath());
        File serverFile = new File(pathFile,uuid+"_"+file.getOriginalFilename());
        file.transferTo(serverFile);

        String imgpath = ("../../static/images/"+uuid+"_"+file.getOriginalFilename()).replace("\\", "/");
        System.out.println(imgpath);
        Map<String,Object> map2=new HashMap<>();
        Map<String,Object> map=new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("data",map2);
        map2.put("src",imgpath);
        return map;
    }

    //处理addadfilm页面提交的数据
    @RequestMapping("/addadfilm")
    public String Addfilm(Adfilms adfilms) throws Exception {
        System.out.println(adfilms);
        adfilmService.addAdfilm(adfilms);
        return "redirect:/administrators/toadmain";
    }

    //返回管理员更新adfilm页面
    @RequestMapping("/toupdateadfilm")
    public String toUpdateadfilm() {
        return "updateadfilm";
    }

    //处理admain页面提搜索查询
    @RequestMapping("/queryadfilm")
    public String queryadfilm(String adName ,String adfilmName,Model model){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("adName",adName);
        map.put("adfilmName",adfilmName);
        Adfilms adfilms = adfilmService.queryAdfilmName(map);
        List<Adfilms> queryadfilmlist = new ArrayList<Adfilms>();
        queryadfilmlist.add(adfilms);
        System.out.println("======="+queryadfilmlist+"========");
        if (adfilms==null){
            queryadfilmlist = adfilmService.queryAllAdfilm(adName);
            model.addAttribute("error","未查到");
        }
        model.addAttribute("querylist", queryadfilmlist);
        return "forward:/administrators/toupdateadfilm";
    }

    //处理updateadfilm页面弹窗form表单的更新
    @RequestMapping("/updateadfilm")
    public String updateadfilm(Adfilms adfilm,Model model){
        System.out.println("******************");
        System.out.println(adfilm);
        adfilmService.updateAdfilm(adfilm);
//        Adfilms adfilms = adfilmService.queryAdfilmById(adfilm.getAdfilmId());
//        List<Adfilms> queryadfilmlist = new ArrayList<Adfilms>();
//        queryadfilmlist.add(adfilms);
//        model.addAttribute("querylist", queryadfilmlist);
        return "forward:/administrators/toupdateadfilm";
    }

    //admian中评论管理adcomment页面 table数据表格 返回layUI中table需要的json数据格式
    @ResponseBody
    @RequestMapping("/tableadcomment")
    public LayuiTypeJson<Comments> tableadcomment(HttpSession session, Model model){
        String adName = (String) session.getAttribute("adName");
        List<Comments> tableadcomment = adfilmService.queryAllComment(adName);
        System.out.println("**************");
        System.out.println(tableadcomment);
        System.out.println("**************");
        //转换layUI中table需要的json数据格式
        LayuiTypeJson<Comments> layuiTypeJson = new LayuiTypeJson<>();
        layuiTypeJson.setCount(tableadcomment.size());
        layuiTypeJson.setData(tableadcomment);
        return layuiTypeJson;
    }

    @RequestMapping("/toadcomment")
    public String toadcomment(HttpSession session, Model model){
        String adName = (String) session.getAttribute("adName");

        return "adcomment";
    }
}
