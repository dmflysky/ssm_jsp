package com.ssm.controller;

import com.ssm.pojo.Adfilms;
import com.ssm.pojo.Comments;
import com.ssm.pojo.User;
import com.ssm.service.CommentService;
import com.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    @Qualifier("UserServiceImpl")
    private UserService userService;

    @Autowired
    @Qualifier("CommentServiceImpl")
    private CommentService commentService;

    @RequestMapping("/tologin")
    public String toLogin() {
        return "login";
    }

    @RequestMapping("/tomain")
    public String toMain(HttpSession session,Model model) {
        //获取session中用户名
        String userName = (String) session.getAttribute("username");
        //获取所有已评论的影评，返回list集合
        List<Comments> userqueryalllist = commentService.queryAllComment(userName);
        //获取所有未评论的影评，返回list集合
        List<Adfilms> userqueryallnolist = commentService.queryAllnoComment(userName);
        //获取有未评论的影评的数量
        int userallnolist = userqueryallnolist.size();
        model.addAttribute("userqueryalllist", userqueryalllist);
        model.addAttribute("userqueryallnolist", userqueryallnolist);
        model.addAttribute("userallnolist", userallnolist);
        return "main";
    }

    //用户登陆提交
    @RequestMapping("/index")
    public String index(HttpSession session, String username, String userpassword, Model model) throws Exception {
        // 向session记录用户身份信息
        session.setAttribute("username", username);
        session.setAttribute("userpassword", userpassword);
        User user = userService.queryUserName(username);
        List<User> list = new ArrayList<User>();
        list.add(user);
        if (user == null) {
            model.addAttribute("usererror", "账号不存在，请你注册或重新输入");
            return "forward:/index.jsp";
        } else if (user.getUserPassword().equals(userpassword)) {
            return "redirect:/user/tomain";
        } else {
            model.addAttribute("pwerror", "密码错误，请你重新输入");
            return "forward:/index.jsp";
        }
    }

    //用户注册
    @RequestMapping("/register")
    public String register(User user) throws Exception {
        System.out.println(user);
        userService.addUser(user);
        return "forward:/index.jsp";
    }

    //退出登陆
    @RequestMapping("logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.invalidate();
        return "redirect:/index.jsp";
    }

    //跳转到addcomment页面
    @RequestMapping("/toaddcomment")
    public String toaddComment(Model model, int adfilmId) {
        Adfilms adfilmById = commentService.queryAdfilmById(adfilmId);
//        System.out.println("-----------"+adfilmById+"------------");
        model.addAttribute("queryadfilmlist", adfilmById);
        return "addcomment";
    }

    //添加comment
    @RequestMapping("/addcomment")
    public String addComment(Comments comments) {
//        System.out.println(comments);
        commentService.addComment(comments);
        return "redirect:/user/tomain";
    }

    //跳转到updatecomment页面
    @RequestMapping("/toupdatecomment")
    public String toupdateComment(Model model, int commentId) {
        Comments commentById = commentService.queryCommentById(commentId);
        System.out.println("============="+commentById+"===========");
        model.addAttribute("querycommentlist", commentById);
        return "updatecomment";
    }

    //更新comment
    @RequestMapping("/updatecomment")
    public String updateComment(Comments comments,Model model) {
        System.out.println("***************"+comments+"***********");
        commentService.updateComment(comments);
        return "redirect:/user/tomain";
    }

    //删除comment
    @RequestMapping("/deletecomment/{commentId}")
    public String deleteComment(@PathVariable("commentId") int commentId) {
        commentService.deleteCommentById(commentId);
        return "redirect:/user/tomain";
    }
}
