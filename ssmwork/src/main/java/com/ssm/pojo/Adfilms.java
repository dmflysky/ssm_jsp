package com.ssm.pojo;
import java.sql.Date;

public class Adfilms {
    private String adName;
    private int adfilmId;
    private String adfilmName;
    private String adfilmText;
    private String adfilmImg;
    private Date adfilmTime;

    public Adfilms() {
    }

    public Adfilms(String adName, int adfilmId, String adfilmName, String adfilmText, String adfilmImg, Date adfilmTime) {
        this.adName = adName;
        this.adfilmId = adfilmId;
        this.adfilmName = adfilmName;
        this.adfilmText = adfilmText;
        this.adfilmImg = adfilmImg;
        this.adfilmTime = adfilmTime;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public int getAdfilmId() {
        return adfilmId;
    }

    public void setAdfilmId(int adfilmId) {
        this.adfilmId = adfilmId;
    }

    public String getAdfilmName() {
        return adfilmName;
    }

    public void setAdfilmName(String adfilmName) {
        this.adfilmName = adfilmName;
    }

    public String getAdfilmText() {
        return adfilmText;
    }

    public void setAdfilmText(String adfilmText) {
        this.adfilmText = adfilmText;
    }

    public String getAdfilmImg() {
        return adfilmImg;
    }

    public void setAdfilmImg(String adfilmImg) {
        this.adfilmImg = adfilmImg;
    }

    public Date getAdfilmTime() {
        return adfilmTime;
    }

    public void setAdfilmTime(Date adfilmTime) {
        this.adfilmTime = adfilmTime;
    }

    @Override
    public String toString() {
        return "Adfilms{" +
                "adName='" + adName + '\'' +
                ", adfilmId=" + adfilmId +
                ", adfilmName='" + adfilmName + '\'' +
                ", adfilmText='" + adfilmText + '\'' +
                ", adfilmImg='" + adfilmImg + '\'' +
                ", adfilmTime=" + adfilmTime +
                '}';
    }
}
