package com.ssm.pojo;

import java.sql.Date;

public class Comments {
    private String userName;
    private int commentId;
    private String commentDiscuss;
    private double commentScore;
    private Date commentTime;
    private String adName;
    private String adfilmName;
    private String adfilmText;
    private String adfilmImg;
    private Date adfilmTime;

    public Comments() {
    }

    public Comments(String userName, int commentId, String commentDiscuss, double commentScore, Date commentTime, String adName, String adfilmName, String adfilmText, String adfilmImg, Date adfilmTime) {
        this.userName = userName;
        this.commentId = commentId;
        this.commentDiscuss = commentDiscuss;
        this.commentScore = commentScore;
        this.commentTime = commentTime;
        this.adName = adName;
        this.adfilmName = adfilmName;
        this.adfilmText = adfilmText;
        this.adfilmImg = adfilmImg;
        this.adfilmTime = adfilmTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentDiscuss() {
        return commentDiscuss;
    }

    public void setCommentDiscuss(String commentDiscuss) {
        this.commentDiscuss = commentDiscuss;
    }

    public double getCommentScore() {
        return commentScore;
    }

    public void setCommentScore(double commentScore) {
        this.commentScore = commentScore;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdfilmName() {
        return adfilmName;
    }

    public void setAdfilmName(String adfilmName) {
        this.adfilmName = adfilmName;
    }

    public String getAdfilmText() {
        return adfilmText;
    }

    public void setAdfilmText(String adfilmText) {
        this.adfilmText = adfilmText;
    }

    public String getAdfilmImg() {
        return adfilmImg;
    }

    public void setAdfilmImg(String adfilmImg) {
        this.adfilmImg = adfilmImg;
    }

    public Date getAdfilmTime() {
        return adfilmTime;
    }

    public void setAdfilmTime(Date adfilmTime) {
        this.adfilmTime = adfilmTime;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "userName='" + userName + '\'' +
                ", commentId=" + commentId +
                ", commentDiscuss='" + commentDiscuss + '\'' +
                ", commentScore=" + commentScore +
                ", commentTime=" + commentTime +
                ", adName='" + adName + '\'' +
                ", adfilmName='" + adfilmName + '\'' +
                ", adfilmText='" + adfilmText + '\'' +
                ", adfilmImg='" + adfilmImg + '\'' +
                ", adfilmTime=" + adfilmTime +
                '}';
    }
}
