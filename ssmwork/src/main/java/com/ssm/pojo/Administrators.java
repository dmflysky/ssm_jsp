package com.ssm.pojo;

public class Administrators {
    private int adId;
    private String adName;
    private String adPassword;

    public Administrators() {
    }

    public Administrators(int adId, String adName, String adPassword) {
        this.adId = adId;
        this.adName = adName;
        this.adPassword = adPassword;
    }

    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdPassword() {
        return adPassword;
    }

    public void setAdPassword(String adPassword) {
        this.adPassword = adPassword;
    }

    @Override
    public String toString() {
        return "Administrators{" +
                "adId=" + adId +
                ", adName='" + adName + '\'' +
                ", adPassword='" + adPassword + '\'' +
                '}';
    }
}
